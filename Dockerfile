# aicacia/build-agent-elixir:${ELIXIR_VERSION}

FROM aicacia/docker-kube-helm:19.03-1.17-3.1.2

ARG ELIXIR_VERSION=1.8.2
ENV ELIXIR_VERSION ${ELIXIR_VERSION}

ENV ENV LANG=C.UTF-8

RUN apt install wget unzip -y
RUN wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb && dpkg -i erlang-solutions_1.0_all.deb
RUN apt update
RUN apt install esl-erlang -y
RUN wget https://github.com/elixir-lang/elixir/releases/download/v${ELIXIR_VERSION}/Precompiled.zip
RUN unzip Precompiled.zip -d elixir
RUN cd elixir/bin && cp elixir elixirc iex mix /usr/local/bin/
RUN cd elixir/lib && cp -R * /usr/local/lib
RUN cd elixir/man && cp * /usr/local/man
RUN rm -rf elixir